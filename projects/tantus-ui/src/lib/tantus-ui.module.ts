import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TantusUiComponent } from './tantus-ui.component';
import { ButtonModule } from 'projects/tantus-ui/src/lib/button/button.module';
export { ButtonModule } from 'projects/tantus-ui/src/lib/button/button.module';

const TANTUS_MODULES = [
  ButtonModule
]

@NgModule({
  declarations: [TantusUiComponent],
  imports: [
    CommonModule,
    TANTUS_MODULES
  ],
  exports: [TANTUS_MODULES]
})
export class TantusUiModule { }
