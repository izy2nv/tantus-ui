import { TestBed } from '@angular/core/testing';

import { TantusUiService } from './tantus-ui.service';

describe('TantusUiService', () => {
  let service: TantusUiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TantusUiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
