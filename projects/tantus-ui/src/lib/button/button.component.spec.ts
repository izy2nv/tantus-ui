import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';

import { ButtonComponent } from './button.component';

describe('ButtonComponent', () => {
  let component: ButtonComponent;
  let fixture: ComponentFixture<ButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  it('should click button', fakeAsync(() => {
    spyOn(component, 'onClickButton');
    fixture.detectChanges();
    let button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    tick();
    fixture.whenStable().then(() => {
      expect(component.onClickButton).toHaveBeenCalled();
    });
  }));
});
