import { Component, OnInit, Input, Output, HostBinding, HostListener, ChangeDetectionStrategy, EventEmitter } from '@angular/core';

@Component({
  selector: 'tantus-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonComponent implements OnInit {
  @Input() label: string;
  @Input() type;
  @Input() variation;
  @Input() size;
  @Input() id;
  @Input() disabled;
  @Output() onClick = new EventEmitter<any>();
  btnEl;
  constructor() { }

  ngOnInit(): void {
    let parentEl = <HTMLInputElement>document.getElementById(this.id);
    if (parentEl !== null) {
      this.btnEl = parentEl.children[0];
    }
    if (this.size !== undefined && this.size.length) {
      this.btnEl.classList.add(this.size);
    }
    if (this.id !== undefined && this.id.length) {
      this.btnEl.id = this.id;
    }
    if (this.disabled !== undefined) {
      this.disabled == 'true' ? this.btnEl.disabled = true: this.btnEl.disabled = false;
    }
  }

  onClickButton(event) {
    this.onClick.emit(event);
  }

}
