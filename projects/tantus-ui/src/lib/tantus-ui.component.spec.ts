import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TantusUiComponent } from './tantus-ui.component';

describe('TantusUiComponent', () => {
  let component: TantusUiComponent;
  let fixture: ComponentFixture<TantusUiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TantusUiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TantusUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
