/*
 * Public API Surface of tantus-ui
 */

export * from './lib/tantus-ui.service';
export * from './lib/tantus-ui.component';
export * from './lib/tantus-ui.module';
export * from './lib/button/button.module';
