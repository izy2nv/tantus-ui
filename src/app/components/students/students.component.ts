import { Component, OnInit } from '@angular/core';
import studentsRecords from '../../../models/students.json';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.scss']
})
export class StudentsComponent implements OnInit {
  students: any = [];
  rowGroupMetadata: any;
  cols: any;
  moreThan3: string = 'Students with more than 3 courses:';
  lessThan4: string = 'Students with 3 or less courses:';
  tableDesc: string = 'Table of students with and without course assignments';
  constructor() { }

  ngOnInit(): void {
    this.cols = [{ field: 'name', header: 'Name' },
    { field: '', header: 'Number of Courses' },
    { field: 'courses', header: 'Courses' }];
    // Fetch students data
    this.getAllStudents();
    this.updateRowGroupMetaData();
  }

  getAllStudents() {
    this.students = studentsRecords.students;
  }

  onSort() {
    this.updateRowGroupMetaData();
  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};

    if (this.students) {
      for (let i = 0; i < this.students.length; i++) {
        this.students[i].greaterThan3 = this.students[i]['courses'].length > 3 ? true : false;
        let rowData = this.students[i];
        let isMorethan3 = this.students[i].greaterThan3;


        if (i == 0) {
          this.rowGroupMetadata[isMorethan3] = { index: 0, size: 1 };
        }
        else {
          let previousRowData = this.students[i - 1];
          let previousRowGroup = previousRowData.greaterThan3;
          if (isMorethan3 === previousRowGroup)
            this.rowGroupMetadata[isMorethan3].size++;
          else
            this.rowGroupMetadata[isMorethan3] = { index: i, size: 1 };
        }
      }
    }
  }

  viewStudents(viewAll: boolean) {
    console.log('testing');
    this.students = viewAll ? studentsRecords.students : this.students.filter(obj => obj.courses.length > 0);
  }
}
